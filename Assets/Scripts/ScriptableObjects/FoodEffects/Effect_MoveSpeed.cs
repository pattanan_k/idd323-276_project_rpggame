using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Effect/SpeedEffect")]
public class Effect_MoveSpeed : FoodEffect
{
    public int speedIncrease;
    public float speedDuration;

    public override void Activate()
    {
        GameObject.FindObjectOfType<Player>().StartExternalCoroutine(SpeedEffect());
    }

    public IEnumerator SpeedEffect()
    {
        FindObjectOfType<Player>().moveSpeed += speedIncrease;
        yield return new WaitForSeconds(speedDuration);
        FindObjectOfType<Player>().moveSpeed -= speedIncrease;
    }
}
