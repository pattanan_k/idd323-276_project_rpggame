using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Effect/DefenseEffect")]
public class Effect_DefUp : FoodEffect
{
    public int defIncrease;
    public float defDuration;

    public override void Activate()
    {
        GameObject.FindObjectOfType<Player>().StartExternalCoroutine(DefenseEffect());
    }

    public IEnumerator DefenseEffect()
    {
        FindObjectOfType<HP>().defense += defIncrease;
        yield return new WaitForSeconds(defDuration);
        FindObjectOfType<HP>().defense -= defIncrease;
    }
}
