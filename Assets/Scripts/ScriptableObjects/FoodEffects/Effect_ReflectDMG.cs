using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Effect/ReflectEffect")]
public class Effect_ReflectDMG : FoodEffect
{
    public float reflectDuration;

    public override void Activate()
    {
        GameObject.FindObjectOfType<Player>().StartExternalCoroutine(ReflectEffect());
    }

    public IEnumerator ReflectEffect()
    {
        FindObjectOfType<HP>().reflectDMG = true;
        yield return new WaitForSeconds(reflectDuration);
        FindObjectOfType<HP>().reflectDMG = false;
    }
}
