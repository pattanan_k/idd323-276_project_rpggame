using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Effect/HealEffect")]
public class Effect_Heal : FoodEffect
{
    public int healAmount = 20;

    public override void Activate()
    {
        FindObjectOfType<HP>().Heal(healAmount);
    }
}
