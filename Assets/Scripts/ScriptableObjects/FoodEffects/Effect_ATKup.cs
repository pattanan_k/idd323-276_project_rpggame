using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Effect/ATKEffect")]
public class Effect_ATKup : FoodEffect
{
    public int atkIncrease;
    public float buffDuration;

    public override void Activate()
    {
        GameObject.FindObjectOfType<Player>().StartExternalCoroutine(ATKupEffect());
    }

    public IEnumerator ATKupEffect()
    {
        FindObjectOfType<Player>().playerATK += atkIncrease;
        yield return new WaitForSeconds(buffDuration);
        FindObjectOfType<Player>().playerATK -= atkIncrease;
    }
}
