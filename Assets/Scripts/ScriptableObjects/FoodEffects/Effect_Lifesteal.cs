using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Effect/LifestealEffect")]
public class Effect_Lifesteal : FoodEffect
{
    public float buffDuration;

    public override void Activate()
    {
        GameObject.FindObjectOfType<Player>().StartExternalCoroutine(LifestealEffect());
    }

    public IEnumerator LifestealEffect()
    {
        FindObjectOfType<HP>().lifeSteal = true;
        yield return new WaitForSeconds(buffDuration);
        FindObjectOfType<HP>().lifeSteal = false;
    }
}
