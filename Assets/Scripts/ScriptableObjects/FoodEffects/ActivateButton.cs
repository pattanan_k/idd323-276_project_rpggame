using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateButton : MonoBehaviour
{
    public int foodAmount;
    public List<FoodEffect> EffectList = new List<FoodEffect>();
    public Food_ScriptableObject food;

    public void ActivateEffect()
    {
        if (foodAmount -1 >= 0)
        {
            if (FindObjectOfType<Player>().canConsume(food.hunger))
            {
                for (int i = 0; i < EffectList.Count; i++)
                {
                    EffectList[i].Activate();
                }

                FindObjectOfType<InventoryDisplay>().UseFood(food);
                FindObjectOfType<Player>().AddHunger(food.hunger);
            }
        }
    }
}
