using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Effect/RegenEffect")]
public class Effect_Regen : FoodEffect
{
    public int regenAmount;
    public float regenInterval;

    public override void Activate()
    {
        GameObject.FindObjectOfType<Player>().StartExternalCoroutine(RegenEffect());
    }

    public IEnumerator RegenEffect()
    {
        for (int i = 0; i < 5; i++)
        {
           FindObjectOfType<HP>().Heal(regenAmount);
           yield return new WaitForSeconds(regenInterval);
        }
    }
}
