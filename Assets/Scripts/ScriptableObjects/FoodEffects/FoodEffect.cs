using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class FoodEffect : ScriptableObject
{
    public abstract void Activate();
}
