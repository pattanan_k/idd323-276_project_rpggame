using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Ingredient : MonoBehaviour
{
    public Inventory inventoryScript;
    public LayerMask player;
    void Start()
    {
        
    }

    void Update()
    {
        if (Physics2D.OverlapCircle(this.gameObject.transform.position, .6f, player))
        {
            addToInventory();
        }
    }

    public abstract void addToInventory();
}
