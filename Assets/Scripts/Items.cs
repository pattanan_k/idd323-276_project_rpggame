using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Items : MonoBehaviour
{
    [SerializeField] private Item_ScriptableObject _itemSO;
    public int itemID { get; private set; }
    public string itemName { get; private set; }
    public string itemDescription { get; private set; }
    private Inventory _inventoryScript;
    [SerializeField] private LayerMask _player;

    private void Start()
    {
        GameObject System = GameObject.Find("System");
        _inventoryScript = System.GetComponent<Inventory>();
        itemID = _itemSO.itemID;
        itemName = _itemSO.itemName;
        itemDescription = _itemSO.itemDescription;
    }

    void Update()
    {
        if (Physics2D.OverlapCircle(this.gameObject.transform.position, .6f, _player))
        {
            AddToInventory();
        }
    }

    void AddToInventory()
    {
        _inventoryScript.inventory.Add(_itemSO);
        Destroy(this.gameObject);
    }
}
