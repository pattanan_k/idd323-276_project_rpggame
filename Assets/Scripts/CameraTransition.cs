using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTransition : MonoBehaviour
{
    enum transitionTo
    {
        left, right, up, down
    }

    public Vector2 newMinPos;
    public Vector2 newMaxPos;
    public Vector2 playerPosChange;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        CameraMovement camMove = Camera.main.gameObject.GetComponent<CameraMovement>();
        Vector2 playerTransform = FindObjectOfType<Player>().transform.localPosition;
        playerTransform += playerPosChange;
        FindObjectOfType<Player>().transform.localPosition = playerPosChange;

        camMove.minPos = newMinPos;
        camMove.maxPos = newMaxPos;
    }
}
