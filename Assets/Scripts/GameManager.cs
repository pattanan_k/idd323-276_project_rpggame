using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MusicChange
{
    Camp, Forest, Beach, Desert, Swamp
}
public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public MusicChange currentMusic
    {
        get
        {
            return _currentMusic;
        }
        set
        {
            if (value != _currentMusic)
            {
                FindObjectOfType<AudioManager>().Stop("Camp");
                FindObjectOfType<AudioManager>().Stop("Forest");
                FindObjectOfType<AudioManager>().Stop("Beach");
                FindObjectOfType<AudioManager>().Stop("Desert");
                FindObjectOfType<AudioManager>().Stop("Swamp");
                switch (value)
                {
                    case MusicChange.Camp:
                        FindObjectOfType<AudioManager>().Play("Camp");
                        break;
                    case MusicChange.Forest:
                        FindObjectOfType<AudioManager>().Play("Forest");
                        break;
                    case MusicChange.Beach:
                        FindObjectOfType<AudioManager>().Play("Beach");
                        break;
                    case MusicChange.Desert:
                        FindObjectOfType<AudioManager>().Play("Desert");
                        break;
                    case MusicChange.Swamp:
                        FindObjectOfType<AudioManager>().Play("Swamp");
                        break;
                    default:
                        break;
                }
                _currentMusic = value;
            }
        }
    }
    private MusicChange _currentMusic;
    public List<GameObject> currentMonsterList = new List<GameObject>();

    private void Awake()
    {
        Load();

        if (GameManager.instance == null)
        {
            GameManager.instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    private void Start()
    {
        FindObjectOfType<AudioManager>().Play("Camp");
    }

    public void ChangeArea(EnemyArea_SO enemyAreaData)
    {
        for (int i = 0; i < currentMonsterList.Count; i++)
        {
            Destroy(currentMonsterList[i]);
        }

        currentMonsterList.Clear();

        for (int i = 0; i < enemyAreaData.Enemies.Count; i++)
        {
            GameObject SpawnEnemy = Instantiate(enemyAreaData.Enemies[i].enemy, enemyAreaData.Enemies[i].enemyPos, 
                                                                                Quaternion.Euler(Vector3.zero));
            currentMonsterList.Add(SpawnEnemy);
        }
    }

    public void Save()
    {
        SaveSystem.SaveItems(FindObjectOfType<Inventory>());
    }

    public void Load()
    {
        SaveData data = SaveSystem.LoadData();

        if (data != null)
        {
            FindObjectOfType<Inventory>().inventory = data.inventory;
            FindObjectOfType<Inventory>().foodInventory = data.foodInventory;
        }
    }
}
