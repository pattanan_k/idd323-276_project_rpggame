using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Enemy/EnemyArea")]
public class EnemyArea_SO : ScriptableObject
{
    public List<EnemyData> Enemies = new List<EnemyData>();
}

[System.Serializable]
public class EnemyData
{
    public Vector3 enemyPos;
    public GameObject enemy;
}
