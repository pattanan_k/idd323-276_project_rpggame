using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InventoryDisplay : MonoBehaviour
{
    [SerializeField] private Inventory inventoryScript;
    [SerializeField] GameObject inventoryUI;
    private bool isActive;

    [Header("Item Counter")]
    [SerializeField] private TextMeshProUGUI slimeDropAmount;
    [SerializeField] private TextMeshProUGUI goblinMeatAmount;
    [SerializeField] private TextMeshProUGUI eelMeatAmount;
    [SerializeField] private TextMeshProUGUI crabRoeAmount;
    [SerializeField] private TextMeshProUGUI cactusFruitAmount;
    [SerializeField] private TextMeshProUGUI wormSegmentAmount;
    [SerializeField] private TextMeshProUGUI caimanTailAmount;
    [SerializeField] private TextMeshProUGUI leviathanFinAmount;
    public int slimeDropCount { get; private set; }
    public int goblinMeatCount { get; private set; }
    public int eelMeatCount { get; private set; }
    public int crabRoeCount { get; private set; }
    public int cactusFruitCount { get; private set; }
    public int wormSegmentCount { get; private set; }
    public int caimanTailCount { get; private set; }
    public int leviathanFinCount { get; private set; }

    [Header("Food Counter")]
    [SerializeField] private TextMeshProUGUI goblinStewAmount;
    [SerializeField] private TextMeshProUGUI forestPuddingAmount;
    [SerializeField] private TextMeshProUGUI grilledEelAmount;
    [SerializeField] private TextMeshProUGUI seasideDelightAmount;
    [SerializeField] private TextMeshProUGUI cactusJuiceAmount;
    [SerializeField] private TextMeshProUGUI theSaharaAmount;
    [SerializeField] private TextMeshProUGUI crocStirFryAmount;
    [SerializeField] private TextMeshProUGUI lochNessSpecialAmount;
    public int goblinStewCount { get; private set; }
    public int forestPuddingCount { get; private set; }
    public int grilledEelCount { get; private set; }
    public int seasideDelightCount { get; private set; }
    public int cactusJuiceCount { get; private set; }
    public int theSaharaCount { get; private set; }
    public int crocStirFryCount { get; private set; }
    public int lochNessSpecialCount { get; private set; }

    [Header("Food Button")]
    [SerializeField] ActivateButton goblinStewButton;
    [SerializeField] ActivateButton forestPuddingButton;
    [SerializeField] ActivateButton grilledEelButton;
    [SerializeField] ActivateButton seasideDelightButton;
    [SerializeField] ActivateButton cactusJuiceButton;
    [SerializeField] ActivateButton theSaharaButton;
    [SerializeField] ActivateButton crocStirFryButton;
    [SerializeField] ActivateButton lochNessSpecialButton;

    void Start()
    {
        isActive = false;
    }

    void Update()
    {
        UpdateAmountText();

        if (isActive == true)
        {
            inventoryUI.SetActive(true);
        }
        else if (isActive == false)
        {
            inventoryUI.SetActive(false);
        }

        openInventory();
    }

    void AmountText(TextMeshProUGUI _tmp, int _count)
    {
        _tmp.text = $"X {_count}";
    }

    void UpdateAmountText()
    {
        //Ingredient
        AmountText(slimeDropAmount, slimeDropCount);
        AmountText(goblinMeatAmount, goblinMeatCount);
        AmountText(eelMeatAmount, eelMeatCount);
        AmountText(crabRoeAmount, crabRoeCount);
        AmountText(cactusFruitAmount, cactusFruitCount);
        AmountText(wormSegmentAmount, wormSegmentCount);
        AmountText(caimanTailAmount, caimanTailCount);
        AmountText(leviathanFinAmount, leviathanFinCount);

        //Food
        AmountText(goblinStewAmount, goblinStewCount);
        AmountText(forestPuddingAmount, forestPuddingCount);
        AmountText(grilledEelAmount, grilledEelCount);
        AmountText(seasideDelightAmount, seasideDelightCount);
        AmountText(cactusJuiceAmount, cactusJuiceCount);
        AmountText(theSaharaAmount, theSaharaCount);
        AmountText(crocStirFryAmount, crocStirFryCount);
        AmountText(lochNessSpecialAmount, lochNessSpecialCount);

        //Update food quantity
        UpdateFoodQuantity(goblinStewButton, goblinStewCount);
        UpdateFoodQuantity(forestPuddingButton, forestPuddingCount);
        UpdateFoodQuantity(grilledEelButton, grilledEelCount);
        UpdateFoodQuantity(seasideDelightButton, seasideDelightCount);
        UpdateFoodQuantity(cactusJuiceButton, cactusJuiceCount);
        UpdateFoodQuantity(theSaharaButton, theSaharaCount);
        UpdateFoodQuantity(crocStirFryButton, crocStirFryCount);
        UpdateFoodQuantity(lochNessSpecialButton, lochNessSpecialCount);
    }

    void UpdateFoodQuantity(ActivateButton button, int count)
    {
        button.foodAmount = count;
    }

    void openInventory()
    {
        if (Input.GetKeyDown(KeyCode.Tab) && isActive == false)
        {
            isActive = true;
            ResetAmount();
            CountItem();
        }
        else if (Input.GetKeyDown(KeyCode.Tab) && isActive == true)
        {
            isActive = false;
        }
    }

    public void CountItem()
    {
        foreach (Item_ScriptableObject item in inventoryScript.inventory)
        {
            switch (item.itemID)
            {
                case 0:
                    slimeDropCount += 1;
                    break;

                case 1:
                    goblinMeatCount += 1;
                    break;

                case 2:
                    eelMeatCount += 1;
                    break;

                case 3:
                    crabRoeCount += 1;
                    break;

                case 4:
                    cactusFruitCount += 1;
                    break;

                case 5:
                    wormSegmentCount += 1;
                    break;

                case 6:
                    caimanTailCount += 1;
                    break;

                case 7:
                    leviathanFinCount += 1;
                    break;
            }
        }

        foreach (Food_ScriptableObject food in inventoryScript.foodInventory)
        {
            switch (food.foodID)
            {
                case 0:
                    goblinStewCount += 1;
                    break;

                case 1:
                    forestPuddingCount += 1;
                    break;

                case 2:
                    grilledEelCount += 1;
                    break;

                case 3:
                    seasideDelightCount += 1;
                    break;

                case 4:
                    cactusJuiceCount += 1;
                    break;

                case 5:
                    theSaharaCount += 1;
                    break;

                case 6:
                    crocStirFryCount += 1;
                    break;

                case 7:
                    lochNessSpecialCount += 1;
                    break;
            }
        }
    }

    public void ResetAmount()
    {
        //Ingredient
        slimeDropCount = 0;
        goblinMeatCount = 0;
        eelMeatCount = 0;
        crabRoeCount = 0;
        cactusFruitCount = 0;
        wormSegmentCount = 0;
        caimanTailCount = 0;
        leviathanFinCount = 0;

        //Food
        goblinStewCount = 0;
        forestPuddingCount = 0;
        grilledEelCount = 0;
        seasideDelightCount = 0;
        cactusJuiceCount = 0;
        theSaharaCount = 0;
        crocStirFryCount = 0;
        lochNessSpecialCount = 0;
    }

    public void UseFood(Food_ScriptableObject food)
    {
        inventoryScript.foodInventory.Remove(food);
        ResetAmount();
        CountItem();
    }


    //void AmountText(TextMeshProUGUI _tmp, string _name, int _count)
    //{
    //    _tmp.text = $"{_name} X {_count}";
    //}

    //void UpdateAmountText()
    //{
    //    //Ingredient
    //    AmountText(slimeDropAmount, "Slime Drop", slimeDropCount);
    //    AmountText(goblinMeatAmount, "Goblin Meat", goblinMeatCount);

    //    //Food
    //    AmountText(goblinStewAmount, "Goblin Stew", goblinStewCount);
    //}
}
