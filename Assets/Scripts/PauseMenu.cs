using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    [SerializeField] bool paused;
    [SerializeField] GameObject pauseScreen;

    void Start()
    {
        Time.timeScale = 1;
        paused = false;
        pauseScreen.SetActive(false);
    }

    void Update()
    {
        if (paused == true)
        {
            pauseScreen.SetActive(true);
            Time.timeScale = 0;
        }
        else if (paused == false)
        {
            pauseScreen.SetActive(false);
            Time.timeScale = 1;
        }

        GamePause();
    }

    void GamePause()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && paused == false)
        {
            paused = true;
        }
        else if (Input.GetKeyDown(KeyCode.Escape) && paused == true)
        {
            GameResume();
        }
    }

    public void GameResume()
    {
        paused = false;
    }
}
