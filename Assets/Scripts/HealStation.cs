using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealStation : MonoBehaviour
{
    [SerializeField] SFX SFXScript;
    public AudioClip healSFX;

    public void OnMouseUp()
    {
        GameObject.FindObjectOfType<HP>().playerHP += 50;
        SFXScript.PlaySound(healSFX);
    }
}
