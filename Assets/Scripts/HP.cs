using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HP : MonoBehaviour
{
    [SerializeField] public int playerHP;
    [SerializeField] public int maxHP = 50;
    [SerializeField] TextMeshProUGUI hpText;
    [SerializeField] GameObject gameOver;
    [SerializeField] SFX SFXScript;
    public int defense = 0;
    [SerializeField] bool inIframe = false;
    public bool reflectDMG = false;
    public bool lifeSteal = false;
    public AudioClip damagedSFX;

    private void Start()
    {
        Time.timeScale = 1;
        gameOver.SetActive(false);
    }

    private void Update()
    {
        if (playerHP <= 0)
        {
            playerHP = 0;
            GameOver();
        }

        if (playerHP >= 50)
        {
            playerHP = 50;
        }

        Cheat();

        hpText.text = $"HP:{playerHP}/50";
    }

    public void damaged(int Damage, GameObject enemy)
    {
        if (!inIframe)
        {
            SFXScript.PlaySound(damagedSFX);
            if (reflectDMG)
            {
                enemy.GetComponent<EnemyAI>().enemyHP -= 1;
            }
            playerHP = playerHP - (Damage - defense);
            StartCoroutine(PlayeriFrame());
        }
    }

    public void Heal(int Amount)
    {
        playerHP += Amount;

        if (playerHP >= maxHP)
        {
            playerHP = maxHP;
        }
    }

    public void Lifesteal()
    {
        if (lifeSteal)
        {
            Heal(2);
        }
    }

    void GameOver()
    {
        gameOver.SetActive(true);
        Time.timeScale = 0;
    }

    void Cheat()
    {
        if (Input.GetKeyUp(KeyCode.I) && !inIframe)
        {
            inIframe = true;
        }
        else if (Input.GetKeyUp(KeyCode.I) && inIframe)
        {
            inIframe = false;
        }
    }

    IEnumerator PlayeriFrame()
    {
        inIframe = true;
        GetComponent<SpriteRenderer>().color = new Color(GetComponent<SpriteRenderer>().color.r,
            GetComponent<SpriteRenderer>().color.g, GetComponent<SpriteRenderer>().color.b, 0.5f);
        yield return new WaitForSeconds(1);
        inIframe = false;
        GetComponent<SpriteRenderer>().color = new Color(GetComponent<SpriteRenderer>().color.r,
            GetComponent<SpriteRenderer>().color.g, GetComponent<SpriteRenderer>().color.b, 1f);
    }
}
