using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuSwitch : MonoBehaviour
{
    [SerializeField] GameObject switchTarget1;
    [SerializeField] GameObject switchTarget2;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void Switch()
    {
        switchTarget1.SetActive(false);
        switchTarget2.SetActive(true);
    }
}
