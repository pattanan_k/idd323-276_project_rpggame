using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SaveData
{
    public List<Item_ScriptableObject> inventory = new List<Item_ScriptableObject>();
    public List<Food_ScriptableObject> foodInventory = new List<Food_ScriptableObject>();

    public SaveData (Inventory inv)
    {
        inventory = inv.inventory;
        foodInventory = inv.foodInventory;
    }
}
