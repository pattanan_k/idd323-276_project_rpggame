using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public List<Item_ScriptableObject> inventory = new List<Item_ScriptableObject>();
    public List<Food_ScriptableObject> foodInventory = new List<Food_ScriptableObject>();
}
