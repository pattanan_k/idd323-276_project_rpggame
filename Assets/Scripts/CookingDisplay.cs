using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CookingDisplay : MonoBehaviour
{
    [SerializeField] GameObject cookingUI;
    void Start()
    {
        cookingUI.SetActive(false);
    }

    void Update()
    {
        
    }

    private void OnMouseUp()
    {
        GameObject.FindObjectOfType<InventoryDisplay>().CountItem();
        cookingUI.SetActive(true);
    }
}
