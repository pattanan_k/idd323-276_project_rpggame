using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Player : MonoBehaviour
{
    public float moveSpeed = 5f;
    private Vector3 change;
    private Animator playerAnim;
    private Rigidbody2D playerRigid;
    public int playerATK;
    public int maxHunger;
    public int currentHunger;
    public TextMeshProUGUI hungerText;
    [SerializeField] float maxTimer;
    [SerializeField] float currentTimer;
    [SerializeField] SFX SFXscript;

    [Header("Sound Effects")]
    public AudioClip attackSFX;

    //public AudioClip MoveSFX;


    void Start()
    {
        Application.targetFrameRate = 30;
        playerRigid = GetComponent<Rigidbody2D>();
        playerAnim = GetComponent<Animator>();
        playerAnim.SetBool("isMoving", false);
    }

    void Update()
    {
        hungerText.text = "HUNGER: " + currentHunger + " / " + maxHunger;

        change = Vector3.zero;
        change.x = Input.GetAxisRaw("Horizontal");
        change.y = Input.GetAxisRaw("Vertical");
        animUpdateAndMovement();
        attackAnim();
        HungerCountdown();
    }

    void attackAnim()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            StartCoroutine(attack());
        }
    }

    private IEnumerator attack()
    {
        SFXscript.PlaySound(attackSFX);
        playerAnim.SetBool("isAttacking", true);
        yield return null;
        playerAnim.SetBool("isAttacking", false);
    }

    void animUpdateAndMovement()
    {
        playerMovement();
        if (change != Vector3.zero)
        {
            playerAnim.SetFloat("moveX", change.x);
            playerAnim.SetFloat("moveY", change.y);
            playerAnim.SetBool("isMoving", true);
        }
        else
        {
            playerAnim.SetBool("isMoving", false);
        }
    }

    void playerMovement()
    {
        playerRigid.velocity = change.normalized * moveSpeed * Time.deltaTime;
    }

    public void StartExternalCoroutine(IEnumerator iEnum)
    {
        StartCoroutine(iEnum);
    }

    public void AddHunger(int value)
    {
        currentHunger += value;
    }

    public bool canConsume(int value)
    {
        if (currentHunger + value > maxHunger)
        {
            Debug.Log(currentHunger + value + "false");
            return false;
        }
        else
        {
            Debug.Log(currentHunger + value + "true");
            return true;
        }
    }

    void HungerCountdown()
    {
        if (currentTimer < 0)
        {
            //trigger
            if (currentHunger -5 >= 0)
            {
                currentHunger -= 5;
            }
            currentTimer = maxTimer;
        }
        else
        {
            currentTimer -= Time.deltaTime;
        }
    }
}
