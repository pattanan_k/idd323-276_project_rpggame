using UnityEngine.Audio;
using System;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public Music[] musics;

    void Awake()
    {
        foreach (Music s in musics)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }
    }
    
    public void Play (string name)
    {
        Music s = Array.Find(musics, musics => musics.name == name);
        s.source.Play();
    }

    public void Stop(string name)
    {
        Music s = Array.Find(musics, musics => musics.name == name);
        s.source.Stop();
    }
}
