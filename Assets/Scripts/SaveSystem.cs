using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveSystem 
{
    public static void SaveItems (Inventory _inventory)
    {
        BinaryFormatter formatter = new BinaryFormatter();

        string path = Application.persistentDataPath + "/inventory.inv";
        FileStream file = new FileStream(path, FileMode.Create);

        SaveData save = new SaveData(_inventory);
        string json = JsonUtility.ToJson(save);

        formatter.Serialize(file, json);
        file.Close();
    }

    public static SaveData LoadData()
    {
        string path = Application.persistentDataPath + "/inventory.inv";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream file = new FileStream(path, FileMode.Open);
            string jsonSaveData = formatter.Deserialize(file) as string;

            SaveData data = JsonUtility.FromJson<SaveData>(jsonSaveData);
            file.Close();

            return data;
        }
        else
        {
            Debug.LogError("No save data in" + path);
            return null;
        }
    }
}
