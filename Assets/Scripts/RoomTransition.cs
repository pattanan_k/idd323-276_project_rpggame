using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomTransition : MonoBehaviour
{
    [SerializeField] Vector2 camChange;
    [SerializeField] Vector3 playerChange;
    private CameraMovement cam;
    public EnemyArea_SO enemyData;
    public MusicChange targetMusic;

    void Start()
    {
        cam = Camera.main.GetComponent<CameraMovement>();
    }

    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            cam.minPos += camChange;
            cam.maxPos += camChange;
            collision.transform.position += playerChange;

            GameManager.instance.ChangeArea(enemyData);
            GameManager.instance.currentMusic = targetMusic;
        }
    }
}
