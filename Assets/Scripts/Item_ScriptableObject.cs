using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Items/Ingredient")]
public class Item_ScriptableObject : ScriptableObject
{
    [SerializeField] private int _itemID;
    public int itemID => _itemID;
    [SerializeField] private string _itemName;
    public string itemName => _itemName;
    [SerializeField] private string _itemDescription;
    public string itemDescription => _itemDescription;
}
