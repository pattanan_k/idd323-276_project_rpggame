using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Items/Food")]
public class Food_ScriptableObject : ScriptableObject
{
    [SerializeField] private int _foodID;
    public int foodID => _foodID;
    [SerializeField] private string _foodName;
    public string foodName => _foodName;
    [SerializeField] private string _foodDescription;
    public string foodDescription => _foodDescription;

    public int hunger;

    public List<FoodEffect> EffectList = new List<FoodEffect>();

    public void ActivateEffect()
    {
        for (int i = 0; i < EffectList.Count; i++)
        {
            EffectList[i].Activate();
        }
    }
}
