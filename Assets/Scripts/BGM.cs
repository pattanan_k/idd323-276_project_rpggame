using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// Attach this component to objects that you want to keep alive (e.g. theme songs) in certain scene transitions. 
public class BGM : MonoBehaviour
{
    public List<string> sceneNames;
    public string instanceName;

    private void Start()
    {
        DontDestroyOnLoad(this.gameObject);
        SceneManager.sceneLoaded += OnSceneLoaded;
    }
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        CheckForDuplicateInstance();
        CheckIfSceneInList();
    }
    void CheckForDuplicateInstance()
    {
        BGM[] collection = FindObjectsOfType<BGM>();

        foreach (BGM obj in collection)
        {
            if (obj != this)
            {
                if (obj.instanceName == instanceName)
                {
                    Debug.Log("Duplicate object loaded in scene, deleting now...");
                    DestroyImmediate(obj.gameObject);
                }
            }
        }
    }
    void CheckIfSceneInList()
    {
        string currentScene = SceneManager.GetActiveScene().name;

        if (sceneNames.Contains(currentScene))
        {
            //keep the object alive
        }
        else
        {
            //unsubscribe to the scene load callback
            SceneManager.sceneLoaded -= OnSceneLoaded;
            DestroyImmediate(this.gameObject);
        }
    }
}
