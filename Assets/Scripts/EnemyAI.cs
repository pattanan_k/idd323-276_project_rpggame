using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{
    [SerializeField] float speed;
    [SerializeField] float detectionRange;
    [SerializeField] float attackRange;
    [SerializeField] public int enemyHP;
    [SerializeField] Transform target;
    [SerializeField] LayerMask player;
    [SerializeField] LayerMask playerAttack;
    [SerializeField] GameObject dropItem;
    [SerializeField] GameObject enemySprite;
    private SFX SFXScript;
    private GameObject playerScript;
    public bool Iframe = false;
    public bool targetRight;
    public int enemyATK;
    public AudioClip enemyDamagedSFX;

    void Start()
    {
        target = GameObject.FindWithTag("Player").transform;
        playerScript = GameObject.Find("PlayerCharacter");
        SFXScript = GameObject.FindObjectOfType<SFX>();
    }

    void Update()
    {
        playerDetection();
        //enemyAttack();
        //enemyDamage();

        if (enemyHP <= 0)
        {
            Instantiate(dropItem, transform.position, transform.rotation);
            Destroy(this.gameObject);
        }

        if (targetRight)
        {
            enemySprite.transform.localScale = new Vector3(-1, 1, 1);
        }
        else
        {
            enemySprite.transform.localScale = new Vector3(1, 1, 1);
        }
    }

    void playerDetection()
    {
        if (Vector2.Distance(target.position, transform.position) <= detectionRange)
        {
            //print("Detected.");
            transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
        }

        if (target.position.x > transform.position.x)
        {
            targetRight = true;
        }
        else
        {
            targetRight = false;
        }
    }

    //void enemyAttack()
    //{
    //    if (Physics2D.OverlapCircle(this.gameObject.transform.position, .6f, player))
    //    {
    //        playerScript.GetComponent<HP>().damaged(enemyATK, this.gameObject);
    //    }
    //}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            playerScript.GetComponent<HP>().damaged(enemyATK, this.gameObject);
        }       
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("PlayerAttack"))
        {
            if (Iframe) return;
            SFXScript.PlaySound(enemyDamagedSFX);
            enemyHP -= FindObjectOfType<Player>().playerATK;
            FindObjectOfType<HP>().Lifesteal();
            StartCoroutine(EnemyiFrame());
            Debug.Log(enemyHP);
        }
    }

    //void enemyDamage()
    //{
    //    if (Physics2D.OverlapCircle(this.gameObject.transform.position, .6f, playerAttack))
    //    {
    //        if (Iframe) return;
    //        enemyHP -= FindObjectOfType<Player>().playerATK;
    //        FindObjectOfType<HP>().Lifesteal();
    //        StartCoroutine(EnemyiFrame());
    //        Debug.Log(enemyHP);
    //    }
    //}

    IEnumerator EnemyiFrame()
    {
        Iframe = true;
        GetComponent<SpriteRenderer>().color = new Color(GetComponent<SpriteRenderer>().color.r,
            GetComponent<SpriteRenderer>().color.g, GetComponent<SpriteRenderer>().color.b, 0.5f);
        yield return new WaitForSeconds(0.5f);
        Iframe = false;
        GetComponent<SpriteRenderer>().color = new Color(GetComponent<SpriteRenderer>().color.r,
            GetComponent<SpriteRenderer>().color.g, GetComponent<SpriteRenderer>().color.b, 1f);
    }
}
