using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cooking : MonoBehaviour
{
    private Inventory inventoryScript;
    private InventoryDisplay inventoryDisplayScript;

    [SerializeField] int ingredient1;
    [SerializeField] int ingredient1Amount;

    [SerializeField] int ingredient2;
    [SerializeField] int ingredient2Amount;

    [SerializeField] Button _thisButton;

    private bool _enoughIngredient1;
    private bool _enoughIngredient2;

    [SerializeField] private Item_ScriptableObject _itemToRemove1;
    [SerializeField] private Item_ScriptableObject _itemToRemove2;
    [SerializeField] private Food_ScriptableObject _cookResult;

    void Start()
    {
        GameObject _system = GameObject.Find("System");
        inventoryScript = _system.GetComponent<Inventory>();
        inventoryDisplayScript = _system.GetComponent<InventoryDisplay>();

        _enoughIngredient1 = false;
        _enoughIngredient2 = false;
    }

    void Update()
    {
        IngredientCheck();
        IngredientRequirementMet();
    }

    private void IngredientCheck()
    {
        switch (ingredient1)
        {
            case 0:
                Ingredient1Compare(inventoryDisplayScript.slimeDropCount);
                break;
            case 1:
                Ingredient1Compare(inventoryDisplayScript.goblinMeatCount);
                break;
            case 2:
                Ingredient1Compare(inventoryDisplayScript.eelMeatCount);
                break;
            case 3:
                Ingredient1Compare(inventoryDisplayScript.crabRoeCount);
                break;
            case 4:
                Ingredient1Compare(inventoryDisplayScript.cactusFruitCount);
                break;
            case 5:
                Ingredient1Compare(inventoryDisplayScript.wormSegmentCount);
                break;
            case 6:
                Ingredient1Compare(inventoryDisplayScript.caimanTailCount);
                break;
            case 7:
                Ingredient1Compare(inventoryDisplayScript.leviathanFinCount);
                break;
        }

        switch (ingredient2)
        {
            case 0:
                Ingredient2Compare(inventoryDisplayScript.slimeDropCount);
                break;
            case 1:
                Ingredient2Compare(inventoryDisplayScript.goblinMeatCount);
                break;
            case 2:
                Ingredient2Compare(inventoryDisplayScript.eelMeatCount);
                break;
            case 3:
                Ingredient2Compare(inventoryDisplayScript.crabRoeCount);
                break;
            case 4:
                Ingredient2Compare(inventoryDisplayScript.cactusFruitCount);
                break;
            case 5:
                Ingredient2Compare(inventoryDisplayScript.wormSegmentCount);
                break;
            case 6:
                Ingredient2Compare(inventoryDisplayScript.caimanTailCount);
                break;
            case 7:
                Ingredient2Compare(inventoryDisplayScript.leviathanFinCount);
                break;
        }
    }

    private void Ingredient1Compare(int _inventoryAmount)
    {
        if(ingredient1Amount <= _inventoryAmount)
        {
            _enoughIngredient1 = true;
        }
        else if (ingredient1Amount > _inventoryAmount)
        {
            _enoughIngredient1 = false;
        }
    }

    private void Ingredient2Compare(int _inventoryAmount)
    {
        if (ingredient2Amount <= _inventoryAmount)
        {
            _enoughIngredient2 = true;
        }
        else if (ingredient2Amount > _inventoryAmount)
        {
            _enoughIngredient2 = false;
        }
    }

    private void IngredientRequirementMet()
    {
        if (!_enoughIngredient1 || !_enoughIngredient2)
        {
            _thisButton.interactable = false;
            return;
        }

        _thisButton.interactable = true;
        
        
    }

    public void Cook()
    {
        for(int i = 0; i < ingredient1Amount; i++)
        {
            inventoryScript.inventory.Remove(_itemToRemove1);
        }

        for(int i = 0; i < ingredient2Amount; i++)
        {
            inventoryScript.inventory.Remove(_itemToRemove2);
        }

        inventoryScript.foodInventory.Add(_cookResult);

        inventoryDisplayScript.ResetAmount();
        inventoryDisplayScript.CountItem();
    }
}
